# DTMF Tone Generator utility functions
# Copyright (C) 2018 Doug Freed
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import itertools
import json
import math
import os
from typing import Iterable, Iterator

amplitude = 0.5
sample_rate = 8000


def sine_wave(frequency: int) -> Iterator[float]:
    for i in itertools.count(0):
        yield amplitude * math.sin(2 * math.pi * frequency * i / sample_rate)


def silence() -> Iterator[float]:
    while True:
        yield 0.0

#           1209 Hz 1336 Hz 1477 Hz 1633 Hz
# 697 Hz    1       2       3       A
# 770 Hz    4       5       6       B
# 852 Hz    7       8       9       C
# 941 Hz    *       0       #       D

dtmf_frequencies = {
    '1': (697, 1209),
    '2': (697, 1336),
    '3': (697, 1477),
    'A': (697, 1633),
    '4': (770, 1209),
    '5': (770, 1336),
    '6': (770, 1477),
    'B': (770, 1633),
    '7': (852, 1209),
    '8': (852, 1336),
    '9': (852, 1477),
    'C': (852, 1633),
    '*': (941, 1209),
    '0': (941, 1336),
    '#': (941, 1477),
    'D': (941, 1633),
}


def dtmf(symbol: str) -> Iterator[float]:
    low_frequency, high_frequency = dtmf_frequencies[symbol]
    return map(math.fsum, zip(sine_wave(low_frequency), sine_wave(high_frequency)))


def segment(func: Iterable[float], duration: int) -> Iterator[float]:
    return itertools.islice(func, math.floor(duration / 1000 * sample_rate))


def float_to_lpcm(floats: Iterable[float]) -> Iterator[int]:
    for item in floats:
        yield min(8191, math.floor(item * 8192))


__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
lpcm_to_ulaw_table = json.load(open(os.path.join(__location__, 'ulaw.json')))


def lpcm_to_ulaw(lpcm: Iterable[int]) -> Iterator[int]:
    for item in lpcm:
        yield lpcm_to_ulaw_table[item + 8192]
