# DTMF Tone Generator
# Copyright (C) 2018 Doug Freed
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import itertools
from typing import Iterator

import flask

import dtmf_utils

app = flask.Flask(__name__)


@app.route('/')
def index():
    return 'Hello World!'


def chunk_accumulator(generator: Iterator[int]) -> Iterator[bytes]:
    while True:
        chunk = bytes(itertools.islice(generator, 4096))
        if chunk:
            yield chunk
        else:
            break


@app.route('/<tones>')
def dtmf(tones):
    tone_generators = []
    total_length = 0
    for item in tones.split('-'):
        pieces = item.split(':')
        tones = pieces[0]
        tone_length = 0
        silence_length = 0
        if not tones:
            return 'No tone specified', 400
        if len(pieces) > 1:
            if pieces[1]:
                try:
                    tone_length = int(pieces[1])
                except ValueError:
                    return 'Tone length not integer', 400
            if len(pieces) > 2:
                if pieces[2]:
                    try:
                        silence_length = int(pieces[2])
                    except ValueError:
                        return 'Silence length not integer', 400
        tone_length = max(tone_length, 100)
        silence_length = max(silence_length, 50)
        for tone in tones:
            if tone not in dtmf_utils.dtmf_frequencies:
                return 'Invalid tone', 400
            tone_generators.append(dtmf_utils.segment(dtmf_utils.dtmf(tone), tone_length))
            tone_generators.append(dtmf_utils.segment(dtmf_utils.silence(), silence_length))
            total_length += tone_length + silence_length
        if total_length > 30000:
            return 'Total length exceeded 30 seconds', 400
    audio_stream = dtmf_utils.lpcm_to_ulaw(dtmf_utils.float_to_lpcm(itertools.chain.from_iterable(tone_generators)))
    return flask.Response(chunk_accumulator(audio_stream), mimetype='audio/basic')


if __name__ == '__main__':
    app.run()
